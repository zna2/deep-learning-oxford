# Deep Learning @ Oxford

- Deep Learning Lecture 1 - Introduction.mp4
- Deep Learning Lecture 2 - linear models.mp4
- Deep Learning Lecture 3 - Maximum likelihood and information.mp4
- Deep Learning Lecture 4 - Regularization, model complexity and data complexity (part 1).mp4
- Deep Learning Lecture 5 - Regularization, model complexity and data complexity (part 2).mp4
- Deep Learning Lecture 6 - Optimization.mp4
- Deep learning Lecture 7 - Logistic regression, a Torch approach.mp4
- Deep Learning Lecture 8 - Modular back-propagation, logistic regression and Torch.mp4
- Deep Learning Lecture 9 - Neural networks and modular design in Torch.mp4
- Deep Learning Lecture 10 - Convolutional Neural Networks.mp4
- Deep Learning Lecture 11 - Max-margin learning, transfer and memory networks.mp4
- Deep Learning Lecture 12 - Recurrent Neural Nets and LSTMs.mp4
- Deep Learning Lecture 13 - Alex Graves on Hallucination with RNNs.mp4
- Deep Learning Lecture 14 - Karol Gregor on Variational Autoencoders and Image Generation.mp4
- Deep Learning Lecture 15 - Deep Reinforcement Learning - Policy search.mp4
- Deep Learning Lecture 16 - Reinforcement learning and neuro-dynamic programming.mp4